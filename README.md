## Requirements

* Node v4.4.3 or higher
* npm v3.9.3 or higher

## Quick start

1. Install with `npm install`
2. Run `npm start`
3. Open http://localhost:3000
