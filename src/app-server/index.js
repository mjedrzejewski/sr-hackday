/**
 * APP SERVER
 *
 * Usage:
 *
 * <HOST>:<PORT>/
 *     Verifies user on Auth Server with <session_id> & <session_token> from cookies.
 *     If user is not verified, reCaptcha form is returned instead of application.
 *     In order to serve application, simple page reload is performed after successful reCaptcha verification.
 *
 *     Application displays content of the fetched feed from Feed Server and button, that allows to re-fetch the feed.
 *     If user tries to fetch the feed with an expired token, new reCaptcha verification is required.
 */
var http = require('http');
const parseCookies = require('./parseCookies.js');
const PORT = 3000;
const HOST = process.env.HOST;
const authServer = `http://${HOST}:3001`;

var App = require('./App.js');
var Captcha = require('./Captcha.js');

const siteKey = '6LcJZRkUAAAAAG-2A7pOUsRZGaHDX8MsP_IKnuwJ';

function renderCaptcha() {
    return Captcha(siteKey, authServer);
}

function renderApp(cookies) {
    return App(siteKey, authServer, cookies);
}

function handleRequest(request, response) {
    const cookies = parseCookies(request);
    if (!cookies.session_id || !cookies.session_token) {
        response.end(renderCaptcha());
    }
    else {
        renderApp(cookies).then(app => { response.end(app); });
    }
}

var server = http.createServer(handleRequest);
server.listen(PORT);

