var http = require('http');
const HOST = process.env.HOST;

var Captcha = require('./Captcha.js');

const App = function (siteKey, authServer, cookies) {
    return new Promise(function (resolve, reject) {
        const options = {
            host: HOST,
            port: 3001,
            path: `/id/${cookies.session_id}/token/${cookies.session_token}`,
            method: 'GET'
        };

        const req = http.get(options, function(res) {

            res.setEncoding('utf-8');

            var responseString = '';

            res.on('data', function(data) {
                responseString += data;
            });

            res.on('end', function() {
                const responseObject = JSON.parse(responseString);
                resolve(responseObject.auth ? `
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="g-recaptcha"
      data-sitekey="${siteKey}"
      data-callback="grecaptchaCallback"
      data-size="invisible">
</div>
<script>
grecaptchaCallback = function () {
    const reCaptchaResponse = grecaptcha.getResponse();
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '${authServer}/reCaptcha/'+reCaptchaResponse);
    xhr.onload = function() {
        if (xhr.status === 200) {
            const res = JSON.parse(xhr.responseText);
            document.cookie = "session_id="+res.id;
            document.cookie = "session_token="+res.token;
            getFeed();
        }
    };
    xhr.send();
};

function parseCookies () {
    var list = {},
        rc = document.cookie;

    rc && rc.split(';').forEach(function( cookie ) {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });

    return list;
}

getFeed = function () {
    const cookies = parseCookies();
    document.getElementById('feed').innerHTML='loading...';
    
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://${HOST}:3002/?id='+cookies.session_id+'&token='+cookies.session_token);
    xhr.onload = function() {
        if (xhr.status === 200) {
            const res = JSON.parse(xhr.responseText);
            if (res.auth) document.getElementById('feed').innerHTML=xhr.responseText;
            else {
                grecaptcha.execute();
            }
        }
        else {
            document.getElementById('feed').innerHTML = 'xhr status: ' + xhr.status;
        }
    };
    xhr.send();
};

window.onload = function() {
    getFeed();
};
</script>
Feed content:
<div id="feed"></div>
<button onclick="getFeed()">Get feed!</button>
` : Captcha(siteKey, authServer));
            });
        });
        req.on('error', function (err) { reject(err); });
        req.end();
    });
};

module.exports = App;