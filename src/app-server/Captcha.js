const Captcha = function (siteKey, authServer) {
    return `
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="g-recaptcha"
      data-sitekey="${siteKey}"
      data-callback="grecaptchaCallback"
      data-size="invisible">
</div>
<script>
grecaptchaCallback = function () {
    const reCaptchaResponse = grecaptcha.getResponse();
    const xhr = new XMLHttpRequest();
    xhr.open('GET', '${authServer}/reCaptcha/'+reCaptchaResponse);
    xhr.onload = function() {
        if (xhr.status === 200) {
            const res = JSON.parse(xhr.responseText);
            document.cookie = "session_id="+res.id;
            document.cookie = "session_token="+res.token;
            window.location.reload();
        }
    };
    xhr.send();
};
window.onload = function() {
    grecaptcha.execute();
};
</script>`;
};

module.exports = Captcha;