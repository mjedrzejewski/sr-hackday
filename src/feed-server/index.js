/**
 * FEED SERVER
 *
 * Usage:
 *
 * <HOST>:<PORT>/?id=<session_id>&token=<token_id>
 *     Verifies user on Auth Server and returns simple feed:
 *     {
 *        auth: true|false
 *     }
 */
var http = require('http');
var url = require('url');
const PORT = 3002;
const HOST = process.env.HOST;

function verify(id, token) {
    return new Promise(function (resolve, reject) {
        const options = {
            host: HOST,
            port: 3001,
            path: `/id/${id}/token/${token}`,
            method: 'GET'
        };

        const req = http.get(options, function(res) {

            res.setEncoding('utf-8');

            var responseString = '';

            res.on('data', function(data) {
                responseString += data;
            });

            res.on('end', function() {
                const responseObject = JSON.parse(responseString);
                resolve(responseObject.auth ? '{"auth": true}' : '{"auth": false}');
            });
        });
        req.on('error', function (err) { reject(err); });
        req.end();
    });
}

function handleRequest(request, response) {

    response.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
    });

    const query = url.parse(request.url, true).query;
    verify(query.id, query.token).then(feed => { response.end(feed); });
}

var server = http.createServer(handleRequest);
server.listen(PORT);

