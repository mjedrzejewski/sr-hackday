/**
 * AUTH SERVER
 *
 * Usage:
 *
 * <HOST>:<PORT>/reCaptcha/<reCaptchaResponse>
 *     Verifies user's response on Google reCaptcha API.
 *     Returns:
 *     - on success:
 *     {
 *        auth: true,
 *        id: <session_id>,
 *        token: <session_token>,
 *        expiresAt: <expiration_date>
 *     }
 *     - on failure:
 *     {
 *        auth: false
 *     }
 *
 * <HOST>:<PORT>/id/<session_id>/token/<session_token>
 *     Verifies user's session id & token.
 *     Returns:
 *     - on success:
 *     {
 *        auth: true,
 *        id: <session_id>,
 *        token: <session_token>,
 *        expiresAt: <expiration_date>
 *     }
 *     - on failure:
 *     {
 *        auth: false
 *     }
 *
 */

var querystring = require('querystring');
var http = require('http');
var https = require('https');
const PORT=3001;
const EXPIRATION_TIME = 15; // in seconds

const secretKey = '6LcJZRkUAAAAAF3FCZXW11SL0MfMwdI9q3L_0zND';

const sessions = [null]; // prevent session id = 0
const captchaSessions = {};


const verifyReCaptcha = function (reCaptcha) {

    // prevent using reCaptcha token more than once
    if (captchaSessions[reCaptcha] && sessions[captchaSessions[reCaptcha]].expiresAt > Date.now()) return false;

    return new Promise(function (resolve, reject) {

        const dataString = querystring.stringify({
            'secret': secretKey,
            'response': reCaptcha
        });
        const headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(dataString)
        };
        const options = {
            host: 'www.google.com',
            path: '/recaptcha/api/siteverify',
            method: 'POST',
            headers: headers
        };

        const req = https.request(options, function(res) {
            res.setEncoding('utf-8');

            var responseString = '';

            res.on('data', function(data) {
                responseString += data;
            });

            res.on('end', function() {
                const responseObject = JSON.parse(responseString);
                resolve(responseObject.success ? createSession(reCaptcha) : false);
            });
        });
        req.on('error', function (err) { reject(err); });
        req.write(dataString);
        req.end();
    });
};


const verifyToken = function (id, token) {
    return sessions[id] && (sessions[id].token === token) && (sessions[id].expiresAt > Date.now());
};


const createSession = function (reCaptcha) {
    const session = {
        id: null,
        token: createToken(),
        reCaptcha: reCaptcha,
        expiresAt: Date.now()+1000*EXPIRATION_TIME
    };
    sessions.push(session);
    session.id = sessions.indexOf(session);
    captchaSessions[reCaptcha] = session.id;
    return session;
};

const createToken = function () {
    var hash = 0;
    const s = Date.now().toString();
    for (i = 0; i < s.length; i++) {
        char = s.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash;
    }
    return (hash >>> 0).toString();
};

function handleRequest(request, response){

    response.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
    });
    const result = {auth: false}
    const args = request.url.split('/');

    if (args[1] === 'reCaptcha') {
        const res = verifyReCaptcha(args[2]);
        if (res !== false) {
            res.then(function (session) {
                if (session !== false) {
                    result.auth = true;
                    result.id = session.id;
                    result.token = session.token;
                    result.expiresAt = session.expiresAt;
                }
                response.end(JSON.stringify(result));
            })
            .catch(function (err) {
                console.error(err);
                response.end(JSON.stringify(result));
            });
        } else {
            response.end(JSON.stringify(result));
        }
    } else if (args[1] === 'id' && args[3] === 'token') {
        if (verifyToken(args[2], args[4])) {
            const session = sessions[args[2]];
            result.auth = true;
            result.id = session.id;
            result.token = session.token;
            result.expiresAt = session.expiresAt;
        }
        response.end(JSON.stringify(result));
    }
}


var server = http.createServer(handleRequest);
server.listen(PORT);